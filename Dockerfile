FROM alpine:3.9

MAINTAINER Jo <idetoile@protonmail.com>

RUN apk add --no-cache --update bash nano sudo openssl python3 ca-certificates openssh-client sshpass su-exec ;\
    apk add --no-cache --update --virtual .build-deps python3-dev build-base libffi-dev openssl-dev ;\
    pip3 install --no-cache --upgrade pip ;\
    pip3 install --no-cache --upgrade setuptools ansible cryptography ;\
    apk del --no-cache --purge .build-deps ;\
    rm -rf /var/cache/apk/* ;\
    rm -rf /root/.cache ;\
    ln -s /usr/bin/python3 /usr/bin/python ;

ENV ANSIBLE_FORCE_COLOR 1

RUN mkdir /ansible

WORKDIR /ansible

ENTRYPOINT ["ansible-playbook"]

# default command: display Ansible version
CMD ["--version" ]